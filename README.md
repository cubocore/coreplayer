# CorePlayer
A video and audio player for CuboCore Application Suite. Based on [mpv](https://mpv.io/).

<img src="coreplayer.png" width="500">

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/coreplayer/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* [libcprime](https://gitlab.com/cubocore/libcprime)
* [mpv](https://mpv.io/)

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreApps.Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).
