/*
    *
    * This file is a part of CorePlayer.
    * A media player based on mpv from the CoreApps family
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "coreplayer.h"
#include "ui_coreplayer.h"

#include <QtConcurrent>

#include <cprime/themefunc.h>
#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>
#include <cprime/pinit.h>
#include <cprime/trashmanager.h>
#include <cprime/shareit.h>


static void wakeup( void *ctx ) {

    // This callback is invoked from any mpv thread ( but possibly also
    // recursively from a thread that is calling the mpv API ). Just notify
    // the Qt GUI thread to wake up ( so that it can process events with
    // mpv_wait_event() ), and return as quickly as possible.
    coreplayer *mainwindow = ( coreplayer * )ctx;
    emit mainwindow->mpv_events();
}

coreplayer::coreplayer( QWidget *parent ) :QWidget( parent ),ui( new Ui::coreplayer ) {

    ui->setupUi( this );

    QCoreApplication::instance()->installEventFilter(this);

    loadSettings();
    startSetup();

    createMPV();
}

coreplayer::~coreplayer() {

    delete ui;
    mpv_terminate_destroy( mpv );
    mpv = nullptr;
}

void coreplayer::createMPV() {

    setlocale( LC_NUMERIC, "C" );

    mpv = mpv_create();
    if ( !mpv ) {
        throw std::runtime_error( "failed to create mpv handle" );
    }

    int64_t wid = ui->mpv_container->winId();
    mpv_set_option( mpv, "wid", MPV_FORMAT_INT64, &wid );

    mpv_set_option_string( mpv, "input-default-bindings", "yes" );
    mpv_set_option_string( mpv, "input-vo-keyboard", "no" );
    mpv_set_option_string( mpv, "osc", "yes" );

    mpv_observe_property( mpv, 0, "time-pos", MPV_FORMAT_DOUBLE );
    mpv_observe_property( mpv, 0, "track-list", MPV_FORMAT_NODE );
    mpv_observe_property( mpv, 0, "chapter-list", MPV_FORMAT_NODE );

    connect( this, SIGNAL( mpv_events() ), this, SLOT( handleMpvEvents() ), Qt::QueuedConnection );
    mpv_set_wakeup_callback( mpv, wakeup, this );

    if ( mpv_initialize( mpv ) < 0 )
        throw std::runtime_error( "mpv failed to initialize" );
};

void coreplayer::startSetup() {

    // all toolbuttons icon size in toolBar
    for ( QToolButton *b : ui->toolBar->findChildren<QToolButton *>() ) {
        if ( b ) {
            b->setIconSize( toolBarIconSize );
        }
    }

    ui->mpv_container->setAttribute( Qt::WA_DontCreateNativeAncestors );
    ui->mpv_container->setAttribute( Qt::WA_NativeWindow );
    ui->mpv_container->setFocusPolicy( Qt::StrongFocus );

    QPalette pltt( palette() );
    pltt.setColor( QPalette::Base, Qt::transparent );
    ui->playlist->setPalette( pltt );
    ui->playlist->setFocusPolicy( Qt::NoFocus );
    ui->playlist->setFrameStyle( QFrame::NoFrame );

    // set window size
    if ( touch )
        setWindowState( Qt::WindowMaximized );
    else
        resize( 800, 500 );

    // single click for touchMode
    if( touch )
        connect( ui->playlist, SIGNAL( itemClicked( QListWidgetItem* ) ),this, SLOT( playSelected( QListWidgetItem* ) ) );
    else
        connect( ui->playlist, SIGNAL( itemDoubleClicked( QListWidgetItem* ) ),this, SLOT( playSelected( QListWidgetItem* ) ) );

    connect( ui->addFiles, SIGNAL( clicked() ), this, SLOT( loadFiles() ) );
    connect( ui->openBtn, SIGNAL( clicked() ), this, SLOT( loadFiles() ) );

    ui->pages->setCurrentIndex( 0 );
    ui->playlist->hide();

    ui->info->setToolTip( QString(
		"<center><H3>Shortcuts</H3></center>"
		"<ul>"
		"    <li><b>RIGHT ARROW</b>: Jump ahead 10s</li>"
		"    <li><b>LEFT ARROW</b>: Jump behind 10s</li>"
		"    <li><b>UP ARROW</b>: Increase volume by 10</li>"
		"    <li><b>DOWN ARROW</b>: Decrease volume by 10</li>"
		"    <li><b>M</b>: Mute</li>"
		"    <li><b>N</b>: Play next video of the playlist</li>"
		"    <li><b>P</b>: Play previous video of the playlist</li>"
		"    <li><b>F</b>: Toggle fullscreen</li>"
		"    <li><b>SPACE</b>: Pause/resume playback</li>"
		"</ul>"
    ) );
    ui->info->setPixmap( QIcon::fromTheme( "dialog-information" ).pixmap( toolBarIconSize ) );
};

void coreplayer::loadSettings() {

    toolBarIconSize = sm->value( "CoreApps", "ToolBarIconSize" );
    touch = sm->value( "CoreApps", "TouchMode" );
};

void coreplayer::loadFiles( QStringList paths ) {

    if ( not paths.count() )
		paths = QFileDialog::getOpenFileNames( this, "Add files", QDir::homePath(), "*" );

	if ( not paths.count() )
		return;

	qApp->processEvents();

	createPlaylist( CPrime::ValidityFunc::checkIsValidFile( paths.at( 0 ) ) );
    play( paths.at( 0 ) );
}

void coreplayer::closeEvent( QCloseEvent *event ) {

    event->ignore();
    CPrime::InfoFunc::saveToRecent( "coreplayer", workFilePath );
    event->accept();
};

bool coreplayer::eventFilter( QObject *target, QEvent *event ) {

	if ( event->type() == QEvent::KeyPress ) {

		QKeyEvent *kEvent = dynamic_cast<QKeyEvent *>( event );

		if ( kEvent->key() == Qt::Key_F ) {

			on_fullscreenBtn_clicked();

			event->setAccepted(true);
			return true;
		}

		else if ( ( kEvent->key() == Qt::Key_Escape ) and ( mFullScreen ) ) {

			on_fullscreenBtn_clicked();

			event->setAccepted(true);
			return true;
		}

		else if ( kEvent->key() == Qt::Key_Right ) {

			const char *args[] = { "seek", "10", nullptr };
			mpv_command_async( mpv, 0, args );

			event->setAccepted(true);
			return true;
		}

		else if ( kEvent->key() == Qt::Key_Left ) {

			const char *args[] = { "seek", "-10", nullptr };
			mpv_command_async( mpv, 0, args );

			event->setAccepted(true);
			return true;
		}

		else if ( kEvent->key() == Qt::Key_Up ) {

			mVolume += 10;
			if ( mVolume > 1000 )
				mVolume = 1000;

			mpv_set_property_string( mpv, "volume", QString::number( mVolume ).toLatin1().constData() );

			event->setAccepted(true);
			return true;
		}

		else if ( kEvent->key() == Qt::Key_Down ) {

			mVolume -= 10;
			if ( mVolume < 0 )
				mVolume = -1;

			mpv_set_property_string( mpv, "volume", QString::number( mVolume ).toLatin1().constData() );

			event->setAccepted(true);
			return true;
		}

		else if ( kEvent->key() == Qt::Key_M ) {

			mute = !mute;

			mpv_set_property_string( mpv, "mute", ( mute ? "yes" : "no" ) );

			event->setAccepted(true);
			return true;
		}

		else if ( kEvent->key() == Qt::Key_Space ) {

			pause = !pause;

			mpv_set_property_string( mpv, "pause", ( pause ? "yes" : "no" ) );

			event->setAccepted( true );
			return true;
		}


		/* Play previous video */
		else if ( kEvent->key() == Qt::Key_P ) {

			int idx = ui->playlist->currentRow();

			/* If we are playing the current item */
			if ( idx == 0 )
				playSelected( ui->playlist->item( ui->playlist->count() - 1 ) );

			else
				playSelected( ui->playlist->item( idx - 1 ) );

			event->setAccepted( true );
			return true;
		}

		else if ( kEvent->key() == Qt::Key_N ) {

			int idx = ui->playlist->currentRow();

			/* If we are playing the last item */
			if ( idx == ( ui->playlist->count() - 1 ) )
				playSelected( ui->playlist->item( 0 ) );

			else
				playSelected( ui->playlist->item( idx + 1 ) );

			event->setAccepted( true );
			return true;
		}
	}

	return QWidget::eventFilter( target, event );
};

void coreplayer::createPlaylist( const QString &path ) {

    workFilePath = path;

	QListWidgetItem *item = new QListWidgetItem( QIcon::fromTheme( "video-x-generic" ), QFileInfo( path ).fileName() );
	item->setData( Qt::UserRole, path );
	ui->playlist->addItem( item );
	ui->playlist->setCurrentItem( item, QItemSelectionModel::ClearAndSelect );

    for( QString file: getList( QFileInfo( path ).path() ) ) {
		if ( file == path )
			continue;

        QFileInfo info( file );

		item = new QListWidgetItem( QIcon::fromTheme( "video-x-generic" ), info.fileName() );
		item->setData( Qt::UserRole, info.filePath() );
		ui->playlist->addItem( item );
    }
};

QStringList coreplayer::getList( const QString &path ) {

    QStringList uList;
    QMimeDatabase mDb;

    QDir dir( QFileInfo( path ).isDir() ? path : QFileInfo( path ).path() );

    for ( QString file : dir.entryList( QDir::Files | QDir::NoDotAndDotDot ) ) {
		QMimeType mime = mDb.mimeTypeForFile( file );
		if ( mime.name().startsWith( "audio/" ) or mime.name().startsWith( "video/" ) )
			uList << dir.filePath( file );
    }

    return uList;
};

void coreplayer::play( const QString file ) {

	ui->pages->setCurrentIndex( 1 );
	ui->mpv_container->setFocus();

	workFilePath = file;

    const char *args[] = { "loadfile", file.toUtf8().data(), nullptr };
    mpv_command_async( mpv, 0, args );

    setWindowTitle( QFileInfo( file ).fileName() + " - CorePlayer" );
};

void coreplayer::playSelected( QListWidgetItem *item ) {

	workFilePath = item->data( Qt::UserRole ).toString();

    play( workFilePath );
	ui->playlist->setCurrentItem( item, QItemSelectionModel::ClearAndSelect );
};

void coreplayer::handleMpvEvents() {

    while ( mpv ) {
        mpv_event *event = mpv_wait_event( mpv, 0 );

        if ( event->event_id == MPV_EVENT_NONE )
            break;

        switch ( event->event_id ) {
            case MPV_EVENT_SHUTDOWN: {

				createMPV();
                break;
            }

            case MPV_EVENT_END_FILE: {

                ui->playlist->selectionModel()->clearSelection();
                break;
            }

            default: {

                break;
            }
        }
    }
};

void coreplayer::on_playlistBtn_toggled( bool show ) {

    if ( show )
        ui->playlist->show();

    else
        ui->playlist->hide();
};

void coreplayer::on_infoBtn_clicked() {

    CPrime::AppOpenFunc::defaultAppEngine( CPrime::Category::MetadataViewer, workFilePath, this );
};

void coreplayer::on_fullscreenBtn_clicked() {

    if ( mFullScreen ) {
        ui->toolBar->show();
		showNormal();
		mFullScreen = false;
	}

	else {
        ui->toolBar->hide();
		showFullScreen();
		mFullScreen = true;
	}
};
